.PHONY:
test:
	   mkdir -p .coverage &&\
            go test -v -coverprofile=.coverage/profile ./... &&\
            go tool cover -html=.coverage/profile -o .coverage/coverage.html 

.PHONY:
format:
	   gofmt -s -w .
