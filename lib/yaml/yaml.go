package yaml

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v2"
	"strconv"
)

func Load(input string) (result interface{}, err error) {
	err = yaml.Unmarshal([]byte(input), &result)
	return
}

func Dump(input interface{}) (result string, err error) {
	var r []byte
	r, err = yaml.Marshal(&input)
	if err == nil {
		result = string(r)
	}
	return
}

func PathGet(obj *interface{}, path []interface{}) interface{} {
	var index interface{}
	var current interface{}

	if len(path) > 0 {
		index = path[0]
		path = path[1:]
	}

	switch (*obj).(type) {
	case map[interface{}]interface{}:
		current = (*obj).(map[interface{}]interface{})[index]
	case []interface{}:
		switch index.(type) {
		case int:
			current = (*obj).([]interface{})[index.(int)]
		case string:
			i, err := strconv.Atoi(index.(string))
			if err != nil {
				return nil
			}
			current = (*obj).([]interface{})[i]
		default:
			return nil
		}
	default:
		return nil
	}

	if len(path) == 0 {
		if current == nil {
			return nil
		}
		return current
	}

	return PathGet(&current, path)
}

func PathSet(obj *interface{}, path []interface{}, value interface{}) (err error) {
	var index interface{}
	var current interface{}

	if obj == nil {
		err = errors.New("Object is nil")
		return
	}

	if len(path) > 0 {
		index = path[0]
		path = path[1:]
	}

	if len(path) == 0 {
		switch (*obj).(type) {
		case map[interface{}]interface{}:
			(*obj).(map[interface{}]interface{})[index] = value
		case []interface{}:
			switch index.(type) {
			case int:
				if len((*obj).([]interface{})) <= index.(int) {
					return errors.New("Index out of range")
				}
				(*obj).([]interface{})[index.(int)] = value
			case string:
				i, err := strconv.Atoi(index.(string))
				if err != nil {
					return errors.New(fmt.Sprintf("Invalid index %+v", index))
				}

				if len((*obj).([]interface{})) <= i {
					return errors.New("Index out of range")
				}

				(*obj).([]interface{})[i] = value
			default:
				return errors.New(fmt.Sprintf("PathSet failed, index of type %T not supported", index))
			}
		}
		return nil
	}

	switch (*obj).(type) {
	case map[interface{}]interface{}:
		current = (*obj).(map[interface{}]interface{})[index]
	case []interface{}:
		switch index.(type) {
		case int:
			if len((*obj).([]interface{})) <= index.(int) {
				return errors.New("Index out of range")
			}
			current = (*obj).([]interface{})[index.(int)]
		case string:
			var i int
			i, err = strconv.Atoi(index.(string))
			if err != nil {
				return errors.New(fmt.Sprintf("Invalid index %+v", index))
			}

			if len((*obj).([]interface{})) <= i {
				return errors.New("Index out of range")
			}
			current = (*obj).([]interface{})[i]
		default:
			return errors.New(fmt.Sprintf("PathSet failed, index of type %T not supported", index))
		}
	default:
		return errors.New(fmt.Sprintf("PathSet failed, obj of type %T not supported", *obj))
	}

	return PathSet(&current, path, value)
}
