package yaml

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func readFile(path string) string {
	path = fmt.Sprintf("%s/../../tests/data/%s", os.Getenv("PWD"), path)
	content, err := ioutil.ReadFile(path)

	if err != nil {
		panic(err)
	}
	return string(content)
}

func TestLoad(t *testing.T) {
	f := readFile("app-01-default.yaml")
	obj, err := Load(f)
	if err != nil {
		panic(err)
	}

	got := obj.(map[interface{}]interface{})["project"]
	expected := "app01"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}
}

func TestDump(t *testing.T) {
	var got, expected interface{}
	var dump string
	var err error

	f := readFile("app-01-default.yaml")
	got, err = Load(f)
	if err != nil {
		t.Fatal(err)
	}

	dump, err = Dump(got)
	if err != nil {
		t.Fatal(err)
	}

	expected, err = Load(dump)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(got, expected) {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}
}

func TestPathGet(t *testing.T) {
	var expected interface{}
	var got interface{}

	f := readFile("app-01-default.yaml")
	obj, err := Load(f)
	if err != nil {
		panic(err)
	}

	// Test it gets a value in a hash
	got = PathGet(&obj, []interface{}{"database", "driver"})
	expected = "mysql"

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got.(string))
	}

	// Test it gets a value in a list
	got = PathGet(&obj, []interface{}{"contact", "support", 0})
	expected = "joe@example.com"

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got.(string))
	}

	// Test it gets a value in a list using string as index "0"
	got = PathGet(&obj, []interface{}{"contact", "support", "0"})
	expected = "joe@example.com"

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got.(string))
	}

	// Test it returns nil when passing an invalid  index for the list
	got = PathGet(&obj, []interface{}{"contact", "support", "2.5"})
	expected = nil

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it returns nil for a list, when path contains values that are not string or int
	got = PathGet(&obj, []interface{}{"contact", "support", nil})
	expected = nil

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it returns nil when obj is neither map[interface{}]interface{} nor []interface{}
	var foo interface{} = "foo"
	got = PathGet(&foo, []interface{}{"contact", "support", nil})
	expected = nil

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it returns nil if path is empty
	got = PathGet(&obj, []interface{}{})
	expected = nil

	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}
}

func TestPathSet(t *testing.T) {
	var got interface{}
	var expected interface{}
	var err error
	var obj interface{}

	f := readFile("app-01-default.yaml")
	obj, err = Load(f)
	if err != nil {
		t.Fatal(err)
	}

	// Test it can set value
	err = PathSet(&obj, []interface{}{"project"}, "app02")
	if err != nil {
		t.Fatal(err)
	}

	got = PathGet(&obj, []interface{}{"project"})
	expected = "app02"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it can set a value that's nested
	err = PathSet(&obj, []interface{}{"database", "driver"}, "postgres")
	if err != nil {
		panic(err)
	}

	got = PathGet(&obj, []interface{}{"database", "driver"})
	expected = "postgres"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it can set a value in a list
	err = PathSet(&obj, []interface{}{"contact", "support", 0}, "foo@example.com")
	if err != nil {
		panic(err)
	}

	got = PathGet(&obj, []interface{}{"contact", "support", 0})
	expected = "foo@example.com"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it can set a value in a list with index being string
	err = PathSet(&obj, []interface{}{"contact", "support", "0"}, "bar@example.com")
	if err != nil {
		panic(err)
	}

	got = PathGet(&obj, []interface{}{"contact", "support", 0})
	expected = "bar@example.com"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it can set a value in a list of hashes
	err = PathSet(&obj, []interface{}{"tags", 0, "Project"}, "app02")
	if err != nil {
		panic(err)
	}

	got = PathGet(&obj, []interface{}{"tags", 0, "Project"})
	expected = "app02"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it can set a value in a list of hashes, using string as index
	err = PathSet(&obj, []interface{}{"tags", "0", "Project"}, "app03")
	if err != nil {
		panic(err)
	}

	got = PathGet(&obj, []interface{}{"tags", 0, "Project"})
	expected = "app03"
	if got != expected {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error if setting value in a list where string can't be converted to int
	got = PathSet(&obj, []interface{}{"contact", "support", "5.8"}, "bar@example.com")
	expected = errors.New("Invalid index 5.8")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error if setting value in a list if index goes out of range
	got = PathSet(&obj, []interface{}{"contact", "support", 5}, "bar@example.com")
	expected = errors.New("Index out of range")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error if setting value in a list if index goes out of range unsing string as index
	got = PathSet(&obj, []interface{}{"contact", "support", "5"}, "bar@example.com")
	expected = errors.New("Index out of range")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error if object is nil
	got = PathSet(nil, []interface{}{"database", "driver"}, "postgres")
	expected = errors.New("Object is nil")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error if object is neither map[interface{}]interface nor []interface{}
	var tmp interface{} = "foo"
	got = PathSet(&tmp, []interface{}{"database", "driver"}, "postgres")
	expected = errors.New("PathSet failed, obj of type string not supported")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error setting  value in a list of hashes, when index is neither string nor int
	got = PathSet(&obj, []interface{}{"contact", "support", 3.5}, "app03")
	expected = errors.New("PathSet failed, index of type float64 not supported")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error setting  value in a list of hashes, when index goes out of range
	got = PathSet(&obj, []interface{}{"tags", 3, "Project"}, "app03")
	expected = errors.New("Index out of range")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error setting  value in a list of hashes, using string that can't be converted to int
	got = PathSet(&obj, []interface{}{"tags", "3.5", "Project"}, "app03")
	expected = errors.New("Invalid index 3.5")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error setting  value in a list of hashes, when index goes out of range, using string as index
	got = PathSet(&obj, []interface{}{"tags", "3", "Project"}, "app03")
	expected = errors.New("Index out of range")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

	// Test it gets error setting  value in a list of hashes, when index is neither string nor int
	got = PathSet(&obj, []interface{}{"tags", 3.5, "Project"}, "app03")
	expected = errors.New("PathSet failed, index of type float64 not supported")
	if got.(error).Error() != expected.(error).Error() {
		t.Errorf("Expected: %v, Got: %v", expected, got)
	}

}
